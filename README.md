# scrape.twobm.workorder.demo.data

## Requirements
Check that you have node and npm installed.

To check if you have Node.js installed, run this command in your terminal:

node -v

To confirm that you have npm installed you can run this command in your terminal:

npm -v

## Guide to scrape data
1. Command prompt navigate to directory
2. Run command "npm install" to install required packages.
3. Run command "node scrapeData.js" to start program
4. Type username and password
5. JSON files are stored in path /data

