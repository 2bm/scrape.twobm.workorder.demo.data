const axios = require('axios');
const fs = require('fs');
const Enquirer = require('enquirer');
const enquirer = new Enquirer();
enquirer.register('password', require('prompt-password'));

// variables
var baseUrl = "http://dtl.2bmsap.dk:8000/sap/opu/odata/MWH/WAREHOUSE_SRV/";
var username;
var password;
var entitySets = [];
var downloadedSets = [];
var notDownloadedSets = [];

// questions to be asked
var questions = [
	{
		type: 'input',
		message: 'Enter your MWH username',
		name: 'username'
	}, {
		type: 'password',
		message: 'Enter your password',
		name: 'password'
	}];

// ask for username and password
enquirer.ask(questions)
	.then(function (answers) {
		username = answers.username;
		password = answers.password;
		readBaseUrl();
	});

/*
* Read Base Url
@ Output: all entities
*/
function readBaseUrl() {
	axios({
		method: 'get',
		url: baseUrl,
		auth: { username: username, password: password }
	}).then((response) => {
		if (response.status === 200) {
			// get all entitySets
			entitySets = response.data.d.EntitySets;

			console.log("\nPlease wait for results...\n");

			// run through entity sets
			entitySets.forEach(function (entity) {
				fs.writeFile('data/' + entity + '.json', "" , (err) => {
					});
			});
			

			// run through entity sets
			entitySets.forEach(function (entity) {
				console.log(entity);
				readAndWriteEntitySet(entity, undefined);
			});
		}
	}, (error) => console.log("Your credentials are incorrect. \n\n Please try again ..."));
}

function readAndWriteEntitySet(entity, next) {
	var currentEntity = entity;
	var urlPath = (next === undefined) ? baseUrl + entity + "?$format=json" : next;

	axios({
		method: 'get',
		url: urlPath,
		auth: { username: username, password: password },
	}).then((response) => {
		if (response.status === 200) {
			var result = response.data.d.results;
			if (result.length > 0) {

				// write json file
				fs.appendFile('data/' + currentEntity + '.json',
					JSON.stringify(result, null, 4), (err) => {
						console.log(urlPath)
					});
				
				console.log("Downloaded: " + urlPath);

				//Check if there are more data to fetch for this entity	
				if (response.data.d.__next !== undefined) {
					readAndWriteEntitySet(currentEntity, response.data.d.__next);
				}
			}
		}
	}, (error) => {
		notDownloadedSets.push(currentEntity);
		console.log("Couldn't download the following entity " + urlPath);
	});
}

/*
* Check if all entities have been read and print
*/
function printStatus(entity) {
	var totalSetsRead = downloadedSets.length + notDownloadedSets.length;
	if (totalSetsRead === entitySets.length) {

		// sort sets alphabetically
		downloadedSets.sort();
		notDownloadedSets.sort();

		// print all downloaded sets
		console.log("\nFollowing sets have been downloaded to '/data':");
		downloadedSets.forEach(function (set) {
			console.log(set);
		});

		// print all not downloaded sets
		console.log("\n\nFollowing sets have NOT been downloaded:");
		notDownloadedSets.forEach(function (set) {
			console.log(set);
		});
	}
}