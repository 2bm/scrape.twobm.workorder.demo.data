const axios = require('axios');
const fs = require('fs');
const Enquirer = require('enquirer');
const enquirer = new Enquirer();
enquirer.register('password', require('prompt-password'));

// variables
var baseUrl = "http://id3.2bm.dk:8000/sap/opu/odata/tobmas/WORKORDER_SRV/";
var username;
var password;
var entitySets = [];
var downloadedSets = [];
var notDownloadedSets = [];

// questions to be asked
var questions = [
{
  type: 'input',
  message: 'Enter your ID3 username',
  name: 'username'
}, {
  type: 'password',
  message: 'Enter your password',
  name: 'password'
}];

// ask for username and password
enquirer.ask(questions)
  .then(function(answers) {
    username = answers.username;
    password = answers.password;
    readBaseUrl();
  });

/*
* Read Base Url
@ Output: all entities
*/
function readBaseUrl() {
  axios({ 
    method: 'get', 
    url: baseUrl, 
    auth: { username: username, password: password } 
  }).then((response) => {
    if(response.status === 200) {      
        // get all entitySets
        entitySets = response.data.d.EntitySets;

        console.log("\nPlease wait for results...\n");

        // run through entity sets
        entitySets.forEach(function(entity) {
          readAndWriteEntitySet(entity);
        });
    }
  }, (error) => console.log("Your credentials are incorrect. \n\n Please try again ..."));
}

/*
* Read entity set and write json-file
@ Output: entity json-file
*/
function readAndWriteEntitySet(entity) {
  var currentEntity = entity;
  axios({ 
    method: 'get', 
    url: baseUrl + currentEntity, 
    auth: { username: username, password: password },
    headers: {'Content-Type': 'application/json'}
  }).then((response) => {
      if(response.status === 200) {
            var result = response.data.d.results;
            if(result.length > 0){

              // write json file
              fs.writeFile('data/' + currentEntity + '.json', 
                JSON.stringify(result, null, 4), (err)=> {
                downloadedSets.push(currentEntity);
                printStatus();
              });
          } else{
            notDownloadedSets.push(currentEntity);
            printStatus();
          }
      }
    }, (error) => notDownloadedSets.push(currentEntity), printStatus() );
}

/*
* Check if all entities have been read and print
*/
function printStatus() {
  var totalSetsRead = downloadedSets.length + notDownloadedSets.length;
  if(totalSetsRead === entitySets.length){

    // sort sets alphabetically
    downloadedSets.sort();
    notDownloadedSets.sort();

    // print all downloaded sets
    console.log("\nFollowing sets have been downloaded to '/data':");
    downloadedSets.forEach(function(set) {
      console.log(set);
    });

    // print all not downloaded sets
    console.log("\n\nFollowing sets have NOT been downloaded:");
    notDownloadedSets.forEach(function(set) {
      console.log(set);
    });

  }
}